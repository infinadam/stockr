<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/token', 'TokenController@generate');
Route::post('/token/refresh', 'TokenController@refresh');
Route::delete('/token', 'TokenController@revoke')
    ->middleware('auth:api');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/portfolio/{id?}', 'PortfolioController@index');
    Route::post('/portfolio', 'PortfolioController@store');
    Route::put('/portfolio/{id}', 'PortfolioController@update');
    Route::delete('/portfolio/{id}', 'PortfolioController@destroy');

    Route::get(
        '/portfolio/{pid}/investment/{id?}', 
        'InvestmentController@index');
    Route::post(
        '/portfolio/{pid}/investment', 
        'InvestmentController@store');
    Route::put(
        '/portfolio/{pid}/investment/{id}', 
        'InvestmentController@update');
    Route::delete(
        '/portfolio/{pid}/investment/{id}', 
        'InvestmentController@destroy');
});

Route::get('/symbol/{name}', 'SymbolController@lookup');