<?php

namespace App\Http\Controllers;

use Cookie;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Optimus\ApiConsumer\Router as ApiRouter;


class TokenController extends Controller
{
    const REFRESH_TOKEN = 'refreshToken';

    private $api;

    public function __construct (ApiRouter $router) {
        $this->api = $router;
    }

    public function generate (Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        return $this->proxy('password', [
            'username' => $request->email,
            'password' => $request->password
        ]);
    }

    public function refresh (Request $request) {
        $token = $request->cookie(self::REFRESH_TOKEN);

        return $this->proxy('refresh_token', [
            'refresh_token' => $token
        ]);
    }

    public function revoke (Request $request) {
        $token = Auth::user()->token();

        if (isset($token)) {
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $token->id)
                ->update([ 'revoked' => true ]);
            DB::table('oauth_access_tokens')
                ->where('id', $token->id)
                ->update([ 'revoked' => true ]);
        }

        return response(null, 204)
            ->cookie(Cookie::forget(self::REFRESH_TOKEN));
    }

    private function proxy ($grantType, array $data = []) {
        $client = DB::table('oauth_clients')
            ->where('password_client', 1)
            ->first();

        $res = $this->api->post('/oauth/token', array_merge($data, [
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'grant_type' => $grantType,
            'scope' => '*'
        ]));

        if (!$res->isSuccessful())
            return response(null, 400);

        $data = json_decode($res->getContent());

        return response([
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ])->cookie(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            2592000, // 30 days
            null,
            null,
            false,
            true // HTTP Only
        );
    }
}
