<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SymbolController extends Controller
{
    const BASE_URL = 'https://www.alphavantage.co/query';
    const TYPE = 'TIME_SERIES_DAILY';

    private $client;

    function __construct (Client $httpClient) {
        $this->client = $httpClient;
    }

    public function lookup ($symbol) {
        $req = self::BASE_URL
            .'?function='.self::TYPE
            .'&apikey='.env('ALPHA_VANTAGE_API_KEY')
            .'&symbol='.$symbol
            .'&datatype=csv';

        $data = $this->client->get($req);
        $csv = str_getcsv($data->getBody());

        // 200 Status Code + Error JSON: Quality API right there...
        if (!isset($csv[7]) || !isset($csv[8]) || !isset($csv[9]))
            return response(null, 404);

        return [
            'symbol' => $symbol,
            'bid' => floatval($csv[9]), 
            'high' => floatval($csv[7]), 
            'low' => floatval($csv[8])
        ];
    }
}
