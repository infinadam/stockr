<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\InvestmentRepository;
use App\Investment;

class InvestmentController extends Controller
{
    private $investments;

    public function __construct (InvestmentRepository $investments) {
        $this->investments = $investments;
    }

    public function index ($pid, $id = null) {
        $search = $this->investments->find(Auth::user(), $pid, $id);
        return isset($search) ? $search : response(null, 404);
    }

    public function store (Request $request, $pid) {
        $investment = new Investment();
        $this->validate($request, [
            'symbol' => 'required',
            'strike' => 'required|numeric',
            'shares' => 'required|numeric'
        ]);
        $this->investments->update($investment, $request);
        $res = $this->investments->insert(Auth::user(), $pid, $investment);

        return isset($res) ? $res : response(null, 404);
    }

    public function update (Request $request, $pid, $id) {
        $this->validate($request, [
            'symbol' => 'required',
            'strike' => 'required|numeric',
            'shares' => 'required|numeric'
        ]);
        $investment = $this->investments->find(Auth::user(), $pid, $id);

        if (!$investment) 
            return response(null, 404);
        
        $investment = $this->investments->update($investment, $request);
        $investment->save();
        
        return $investment;
    }

    public function destroy ($pid, $id) {
        $investment = $this->investments->find(Auth::user(), $pid, $id);

        if (!$investment)
            return response(null, 404);
        
        $investment->delete();

        return response(null, 204);
    }
}
