<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\PortfolioRepository;
use App\Portfolio;

class PortfolioController extends Controller
{
    private $portfolios;

    public function __construct (PortfolioRepository $portfolios) {
        $this->portfolios = $portfolios;
    }

    public function index ($id = null) {
        $search = $this->portfolios->find(Auth::user(), $id);
        if (isset($search)) return $search;
        return response(null, 404);
    }

    public function store (Request $request) {
        $this->validate($request, [ 'name' => 'required' ]);
        return $this->portfolios->insert(Auth::user(), $request->name);
    }

    public function update (Request $request, $id) {
        $portfolio = $this->portfolios->find(Auth::user(), $id);

        if (!$portfolio)
            return response(null, 404);
        
        $this->validate($request, [
            'name' => 'required'
        ]);
        $portfolio->name = $request->name;
        $portfolio->save();

        return $portfolio;
    }

    public function destroy ($id) {
        $portfolio = $this->portfolios->find(Auth::user(), $id);

        if (!$portfolio)
            return response(null, 404);

        $portfolio->delete();

        return response(null, 204);
    }
}
