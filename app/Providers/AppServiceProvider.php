<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ProviderRepository;
use App\Repositories\InvestmentRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProviderRepository::class, function () {
            return new ProviderRepository();
        });
        $this->app->bind(InvestmentRepository::class, function () {
            return new InvestmentRepository();
        });
    }
}
