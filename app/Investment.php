<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model {

    protected $fillable = [ 'symbol', 'strike', 'shares', ];
    
    public function portfolio () {
        return $this->belongsTo('App\Portfolio');
    }
}
