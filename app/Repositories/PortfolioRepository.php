<?php

namespace App\Repositories;

use App\Portfolio;

class PortfolioRepository
{
  public function find ($user, $id = null) {
    if (!isset($user)) return null;
    return isset($id) ? $user->portfolios->find($id) : $user->portfolios;
  }

  public function insert ($user, $name) {
    if (!isset($user)) return null;
    $portfolio = new Portfolio([ 'name' => $name ]);
    $user->portfolios()->save($portfolio);
    return $portfolio;
  }
}