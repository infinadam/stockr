<?php

namespace App\Repositories;

use App\Portfolio;

class InvestmentRepository
{
  public function find ($user, $pid, $id = null) {
    if (!isset($user) || !isset($pid)) return null;

    $portfolio = $user->portfolios->find($pid);

    if (!isset($portfolio)) return null;
    
    return isset($id)
      ? $portfolio->investments->find($id) 
      : $portfolio->investments;
  }

  public function update ($investment, $request) {
      $investment->symbol = $request->symbol;
      $investment->strike = $request->strike;
      $investment->shares = $request->shares;

      return $investment;
  }

  public function insert ($user, $pid, $investment) {
    if (!isset($user) || !isset($pid)) return null;
    
    $portfolio = $user->portfolios->find($pid);
    
    if (!isset($portfolio)) return null;

    $portfolio->investments()->save($investment);

    return $investment;
  }
}