<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model {

    protected $fillable = [ 'name', ];

    public function user () {
        return $this->belongsTo('App\User');
    }

    public function investments () {
        return $this->hasMany('App\Investment');
    }
}
