<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class TokenControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_generate_token () {
        $user = factory(User::class)->create();
        $response = $this->post('/api/token', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token', 'expires_in']);
        $response->assertCookie('refreshToken');
    }

    public function test_revoke_token () {
        $user = factory(User::class)->create();
        $response = $this->post('/api/token', [
            'email' => $user->email,
            'password' => 'secret'
        ]);
        $data = json_decode($response->baseResponse->getContent());
        $token = $data->access_token;
        $response = $this
            ->actingAs($user, 'api')
            ->delete('/api/token', [], [
                'HTTP_Authorization' => 'Bearer '.$token
            ]);

        $response->assertStatus(204);
    }

    public function test_refresh_token () {
        $user = factory(User::class)->create();
        $response = $this->post('/api/token', [
            'email' => $user->email,
            'password' => 'secret'
        ]);
        $refresh = $response->headers->getCookies()[0]->getValue();

        $response = $this->call('POST', '/api/token/refresh', [], [
            'refreshToken' => $refresh
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token', 'expires_in']);
        $response->assertCookie('refreshToken');
    }
}
