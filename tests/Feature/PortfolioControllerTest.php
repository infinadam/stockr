<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;

use App\User;
use App\Portfolio;
use App\Repositories\PortfolioRepository;

class PortfolioControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function tearDown () {
        Mockery::close();
    }

    public function test_fetch_all_portfolios ()
    {
        App::bind(PortfolioRepository::class, function () {
            return Mockery::mock(PortfolioRepository::class)
                ->shouldReceive('find')
                ->andReturn([[ 'id' => '1', 'name' => 'test' ]])
                ->getMock();
        });
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')
            ->get('/api/portfolio');
        $response->assertStatus(200);
        $response->assertJson([[ 'id' => '1', 'name' => 'test' ]]);
    }

    public function test_fetch_invalid_portfolio ()
    {
        $user = factory(User::class)->create();
        App::bind(PortfolioRepository::class, function () use ($user) {
            return Mockery::mock(PortfolioRepository::class)
                ->shouldReceive('find')
                ->with($user, 1)
                ->andReturnNull()
                ->getMock();
        });
        
        $response = $this->actingAs($user, 'api')
            ->get('/api/portfolio/1');
        $response->assertStatus(404);
    }

    public function test_create_portfolio ()
    {
        $user = factory(User::class)->create();
        App::bind(PortfolioRepository::class, function () use ($user) {
            return Mockery::mock(PortfolioRepository::class)
                ->shouldReceive('insert')
                ->with($user, 'test')
                ->andReturn([ 'id' => '1', 'name' => 'test' ])
                ->getMock();
        });

        $response = $this->actingAs($user, 'api')
            ->post('/api/portfolio', [ 'name' => 'test' ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([ 'id', 'name' ]);
    }
}
