<?php

namespace Tests\Feature;

use \Mockery;
use Tests\TestCase;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthManager;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;
use App\Portfolio;
use App\Repositories\PortfolioRepository;

class PortfolioRepositoryTest extends TestCase
{
    protected function tearDown () {
        Mockery::close();
    }

    public function test_find_for_all_portfolios () {
        $mockUser = Mockery::mock(User::class);
        $mockUser->shouldReceive('getAttribute')
            ->andReturn([
                [ 'id' => '1', 'name' => 'test' ]
            ]);
        
        $repo = new PortfolioRepository();
        $result = $repo->find($mockUser);

        $this->assertTrue(sizeof($result) == 1);
        $this->assertTrue($result[0] == [ 'id' => '1', 'name' => 'test' ]);
    }

    public function test_find_on_bad_portfolio ()
    {
        $mockUser = Mockery::mock(User::class);
        $mockUser->shouldReceive('getAttribute')
            ->with('portfolios')
            ->andReturn(Mockery::mock()
                ->shouldReceive('find')
                ->andReturnNull()
                ->getMock()
            );
        
        $repo = new PortfolioRepository();
        $result = $repo->find($mockUser, '5');
        $this->assertNull($result);
    }

    public function test_insert_portfolio ()
    {
        $mockUser = Mockery::mock(User::class);
        $mockUser->shouldReceive('portfolios')
            ->andReturn(Mockery::mock()
                ->shouldReceive('save')
                ->andReturn([ 'id' => '1', 'name' => 'test' ])
                ->getMock()
            );
            
        $repo = new PortfolioRepository();
        $result = $repo->insert($mockUser, 'test');
        $this->assertTrue($result instanceof Portfolio);
    }
}
