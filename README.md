# MVC Project for COMP586

## Requirements

This project assumes you have [Docker](https://www.docker.com)
installed, as well as [Docker Compose](https://docs.docker.com/compose/).

## Building

Build and Start the Docker containers for the project with the following
command:

`$ docker-compose up -d`

Finally, run the setup script to populate necessary data:

`$ docker-compose exec stockr ./setup.sh local`

## Launching Application

The web application will be available at
[http://localhost:8080](http://localhost:8080).
