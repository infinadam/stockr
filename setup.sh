#!/bin/bash

cp .env.$1 .env

composer install --no-scripts --no-plugins

php artisan key:gen

php artisan migrate:install
php artisan migrate
php artisan db:seed

php artisan passport:install
