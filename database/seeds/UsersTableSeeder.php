<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(User::class)->create([
            'name' => 'Adam Clark',
            'email' => 'adam.clark.104@my.csun.edu'
        ]);
        factory(User::class)->create([
            'name' => 'Felix Rabinovich',
            'email' => 'felix.rabinovich@csun.edu'
        ]);
    }
}
