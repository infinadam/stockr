webpackJsonp(["main"],{

/***/ "../../../../../angular/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../angular/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../angular/app/api/authentication.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Authentication; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Authentication = (function () {
    function Authentication() {
    }
    return Authentication;
}());
Authentication = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
], Authentication);

//# sourceMappingURL=authentication.js.map

/***/ }),

/***/ "../../../../../angular/app/api/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return API; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var API = (function () {
    function API() {
    }
    return API;
}());
API = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
], API);

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../angular/app/api/resources.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Resources; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalResources; });
// Using instead of interfaces to avoid token injections.
var Resources = (function () {
    function Resources() {
    }
    return Resources;
}());

var LocalResources = {
    token: '/api/token',
    portfolio: '/api/portfolio/{id}',
    investment: '/api/portfolio/{pid}/investment/{id}',
    symbol: '/api/symbol/{name}',
};
//# sourceMappingURL=resources.js.map

/***/ }),

/***/ "../../../../../angular/app/api/services/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APIService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__authentication__ = __webpack_require__("../../../../../angular/app/api/authentication.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resources__ = __webpack_require__("../../../../../angular/app/api/resources.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var APIService = (function () {
    function APIService(http, authentication, resources) {
        this.http = http;
        this.auth = authentication;
        this.resources = resources;
    }
    Object.defineProperty(APIService.prototype, "options", {
        get: function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]()
                .set('Authorization', "Bearer " + this.auth.accessToken);
            return { headers: headers };
        },
        enumerable: true,
        configurable: true
    });
    APIService.prototype.portfolios = function (id) {
        var url;
        if (id) {
            url = this.resources.portfolio.replace('{id}', id.toString());
            return this.http.get(url, this.options);
        }
        url = this.resources.portfolio.replace('/{id}', '');
        return this.http
            .get(url, this.options)
            .flatMap(function (ps) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].from(ps); });
    };
    APIService.prototype.createPortfolio = function (p) {
        var url = this.resources.portfolio.replace('/{id}', '');
        return this.http.post(url, p, this.options);
    };
    APIService.prototype.updatePortfolio = function (p) {
        var url = this.resources.portfolio.replace('{id}', p.id.toString());
        return this.http.put(url, p, this.options);
    };
    APIService.prototype.deletePortfolio = function (id) {
        var url = this.resources.portfolio.replace('{id}', id.toString());
        return this.http.delete(url, this.options);
    };
    APIService.prototype.investments = function (pid) {
        var url = this.resources.investment
            .replace('{pid}', pid.toString())
            .replace('/{id}', '');
        return this.http
            .get(url, this.options)
            .flatMap(function (i) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].from(i); });
    };
    APIService.prototype.search = function (symbol) {
        var url = this.resources.symbol.replace('{name}', symbol);
        return this.http.get(url);
    };
    APIService.prototype.investIn = function (pid, symbol, strike, shares) {
        var url = this.resources.investment
            .replace('{pid}', pid.toString())
            .replace('/{id}', '');
        var investment = { symbol: symbol, strike: strike, shares: shares };
        return this.http.post(url, investment, this.options);
    };
    return APIService;
}());
APIService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__authentication__["a" /* Authentication */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__authentication__["a" /* Authentication */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__resources__["b" /* Resources */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__resources__["b" /* Resources */]) === "function" && _c || Object])
], APIService);

var _a, _b, _c;
//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ "../../../../../angular/app/api/services/authentication.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__types_token__ = __webpack_require__("../../../../../angular/app/api/types/token.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__resources__ = __webpack_require__("../../../../../angular/app/api/resources.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthenticationService = (function () {
    function AuthenticationService(client, resources) {
        this.token = new __WEBPACK_IMPORTED_MODULE_2__types_token__["a" /* Token */]();
        this.client = client;
        this.resources = resources;
    }
    AuthenticationService.prototype.login = function (creds) {
        var _this = this;
        if (this.loggedIn) {
            return Promise.resolve(this.token);
        }
        return new Promise(function (resolve, reject) {
            return _this.client
                .post(_this.resources.token, creds)
                .subscribe(function (data) { return resolve(_this.token.fromApi(data).store()); }, reject);
        });
    };
    AuthenticationService.prototype.logout = function () {
        var _this = this;
        if (!this.loggedIn) {
            return Promise.reject('Not logged in.');
        }
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]()
            .set('Authorization', "Bearer " + this.token.accessToken);
        return new Promise(function (resolve, reject) {
            _this.client
                .delete(_this.resources.token, { headers: headers })
                .subscribe(function () {
                _this.token.forget();
                resolve();
            }, reject);
        });
    };
    Object.defineProperty(AuthenticationService.prototype, "loggedIn", {
        get: function () {
            return this.token.isValid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthenticationService.prototype, "accessToken", {
        get: function () {
            if (this.token.isValid)
                return this.token.accessToken;
            return undefined;
        },
        enumerable: true,
        configurable: true
    });
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__resources__["b" /* Resources */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__resources__["b" /* Resources */]) === "function" && _b || Object])
], AuthenticationService);

var _a, _b;
//# sourceMappingURL=authentication.service.js.map

/***/ }),

/***/ "../../../../../angular/app/api/types/token.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Token; });
var Token = (function () {
    function Token() {
        var data = JSON.parse(localStorage.getItem(Token.storageKey));
        if (data) {
            Object.assign(this, {
                accessToken: data.accessToken,
                expiresAt: new Date(data.expiresAt)
            });
        }
    }
    Token.prototype.fromApi = function (apiToken) {
        this.accessToken = apiToken.access_token;
        this.expiresAt = new Date(Date.now() + apiToken.expires_in);
        return this;
    };
    Token.prototype.store = function () {
        var token = {
            accessToken: this.accessToken,
            expiresAt: this.expiresAt.toString()
        };
        localStorage.setItem(Token.storageKey, JSON.stringify(token));
        return this;
    };
    Token.prototype.forget = function () {
        this.accessToken = this.expiresAt = undefined;
        localStorage.removeItem(Token.storageKey);
    };
    Object.defineProperty(Token.prototype, "isValid", {
        get: function () {
            return !!this.accessToken
                && !!this.expiresAt
                && this.expiresAt.getTime() > Date.now();
        },
        enumerable: true,
        configurable: true
    });
    return Token;
}());

Token.storageKey = 'sr-token';
//# sourceMappingURL=token.js.map

/***/ }),

/***/ "../../../../../angular/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".header, .title {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n\n.logo {\n    font-weight: 100;\n}\n\n.title {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n\n.title .logo {\n    color: darkgreen;\n    display: block;\n    font-size: 200px;\n    margin: 120px 0 0 0;\n    text-align: center;\n    width: 100%;\n}\n\n.header {\n    background-color: darkgreen;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    line-height: 60px;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n\n.header .logo {\n    color: #fafafa;\n    font-size: 40px;\n    margin: 20px;\n    width: 200px;\n}\n\n.login {\n    text-align: center;\n}\n\n.header .login {\n    line-height: 60px;\n    margin: 20px;\n}\n\n.nav {\n    background-color: #eaeaea;\n    padding: 60px 40px 0 40px;\n    width: 400px;\n}\n\n.main {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    min-height: calc(100% - 100px);\n}\n\n.content {\n    -webkit-box-flex: 2;\n        -ms-flex-positive: 2;\n            flex-grow: 2;\n    padding: 60px 40px 0 40px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../angular/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div [class.header]=\"loggedIn\" [class.title]=\"!loggedIn\">\n    <h1 class=\"logo\">stockr</h1>\n    <sr-login class=\"login\"\n      (loggedIn)=\"onAuthChange($event)\"\n    ></sr-login>\n</div>\n\n<div *ngIf=\"loggedIn\" class=\"main\">\n    <div class=\"nav\">\n        <sr-stock (invest)=\"onInvest($event)\"></sr-stock>\n    </div>\n\n    <div class=\"content\">\n        <sr-portfolio (error)=\"onMessage($event)\"></sr-portfolio>\n    </div>\n</div>\n\n<sr-notice></sr-notice>"

/***/ }),

/***/ "../../../../../angular/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_authentication__ = __webpack_require__("../../../../../angular/app/api/authentication.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api__ = __webpack_require__("../../../../../angular/app/api/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_types__ = __webpack_require__("../../../../../angular/app/data/types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(authentication, api, portfolios) {
        this.authentication = authentication;
        this.api = api;
        this.portfolios = portfolios;
    }
    AppComponent.prototype.onAuthChange = function (loggedIn) {
        this.loggedIn = loggedIn;
        if (this.loggedIn)
            this.onLogin();
        else
            this.onLogout();
    };
    AppComponent.prototype.ngOnInit = function () {
        this.loggedIn = this.authentication.loggedIn;
        if (this.loggedIn)
            this.onLogin();
        else
            this.onLogout();
    };
    AppComponent.prototype.onLogin = function () {
        this.portfolios.fill();
    };
    AppComponent.prototype.onLogout = function () { };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'sr-app',
        template: __webpack_require__("../../../../../angular/app/app.component.html"),
        styles: [__webpack_require__("../../../../../angular/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_authentication__["a" /* Authentication */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__api_authentication__["a" /* Authentication */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__api__["a" /* API */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__api__["a" /* API */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__data_types__["a" /* Portfolios */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__data_types__["a" /* Portfolios */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../angular/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../angular/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login_component__ = __webpack_require__("../../../../../angular/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notice_notice_component__ = __webpack_require__("../../../../../angular/app/notice/notice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__portfolio_portfolio_component__ = __webpack_require__("../../../../../angular/app/portfolio/portfolio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__stock_stock_component__ = __webpack_require__("../../../../../angular/app/stock/stock.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__api_authentication__ = __webpack_require__("../../../../../angular/app/api/authentication.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__api_services_authentication_service__ = __webpack_require__("../../../../../angular/app/api/services/authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__api__ = __webpack_require__("../../../../../angular/app/api/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__api_services_api_service__ = __webpack_require__("../../../../../angular/app/api/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__api_resources__ = __webpack_require__("../../../../../angular/app/api/resources.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__data_types__ = __webpack_require__("../../../../../angular/app/data/types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__data_services_portfolios_service__ = __webpack_require__("../../../../../angular/app/data/services/portfolios.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__notice__ = __webpack_require__("../../../../../angular/app/notice/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__notice_notice_service__ = __webpack_require__("../../../../../angular/app/notice/notice.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_6__notice_notice_component__["a" /* NoticeComponent */],
            __WEBPACK_IMPORTED_MODULE_7__portfolio_portfolio_component__["a" /* PortfolioComponent */],
            __WEBPACK_IMPORTED_MODULE_8__stock_stock_component__["a" /* StockComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* ReactiveFormsModule */],
        ],
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_9__api_authentication__["a" /* Authentication */], useClass: __WEBPACK_IMPORTED_MODULE_10__api_services_authentication_service__["a" /* AuthenticationService */] },
            { provide: __WEBPACK_IMPORTED_MODULE_13__api_resources__["b" /* Resources */], useValue: __WEBPACK_IMPORTED_MODULE_13__api_resources__["a" /* LocalResources */] },
            { provide: __WEBPACK_IMPORTED_MODULE_11__api__["a" /* API */], useClass: __WEBPACK_IMPORTED_MODULE_12__api_services_api_service__["a" /* APIService */] },
            { provide: __WEBPACK_IMPORTED_MODULE_14__data_types__["a" /* Portfolios */], useClass: __WEBPACK_IMPORTED_MODULE_15__data_services_portfolios_service__["a" /* PortfoliosService */] },
            { provide: __WEBPACK_IMPORTED_MODULE_16__notice__["a" /* Notice */], useClass: __WEBPACK_IMPORTED_MODULE_17__notice_notice_service__["a" /* NoticeService */] },
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../angular/app/data/services/portfolios.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortfoliosService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_index__ = __webpack_require__("../../../../../angular/app/api/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PortfoliosService = (function () {
    function PortfoliosService(api) {
        this.portfolios = [];
        this.api = api;
    }
    Object.defineProperty(PortfoliosService.prototype, "data", {
        get: function () {
            return this.portfolios;
        },
        enumerable: true,
        configurable: true
    });
    PortfoliosService.prototype.fill = function () {
        var _this = this;
        this.portfolios = [];
        this.api.portfolios()
            .subscribe(function (p) {
            _this.portfolios.push(p);
            p.investments = [];
            _this.api.investments(p.id).subscribe(function (i) {
                p.investments.push(i);
            });
        });
        return this;
    };
    PortfoliosService.prototype.find = function (fn) {
        return this.portfolios.find(fn);
    };
    PortfoliosService.prototype.add = function (p) {
        var _this = this;
        this.api.createPortfolio(p)
            .subscribe(function (data) { return _this.portfolios.push(data); });
        return this;
    };
    PortfoliosService.prototype.update = function (p) {
        var _this = this;
        this.api.updatePortfolio(p)
            .subscribe(function (u) {
            var idx = _this.portfolios.findIndex(function (i) { return i.id == u.id; });
            _this.portfolios.splice(idx, 1, u);
        });
        return this;
    };
    PortfoliosService.prototype.remove = function (p) {
        var _this = this;
        this.api.deletePortfolio(p.id).subscribe(function () {
            var idx = _this.portfolios.findIndex(function (i) { return i.id == p.id; });
            _this.portfolios.splice(idx, 1);
        });
        return this;
    };
    PortfoliosService.prototype.invest = function (p, i) {
        p.investments = p.investments || [];
        this.api.investIn(p.id, i.symbol, i.strike, i.shares)
            .subscribe(function () { return p.investments.push(i); });
        return this;
    };
    return PortfoliosService;
}());
PortfoliosService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_index__["a" /* API */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__api_index__["a" /* API */]) === "function" && _a || Object])
], PortfoliosService);

var _a;
//# sourceMappingURL=portfolios.service.js.map

/***/ }),

/***/ "../../../../../angular/app/data/types.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Portfolios; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");

Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])();
var Portfolios = (function () {
    function Portfolios() {
    }
    return Portfolios;
}());

//# sourceMappingURL=types.js.map

/***/ }),

/***/ "../../../../../angular/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".credentials {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    font-size: 16px;\n    padding-bottom: 40px;\n    position: relative;\n    width: 33%;\n}\n\n.credentials input[type=\"email\"], .credentials input[type=\"password\"] {\n    font-size: 24px;\n    line-height: 50px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n}\n\n.credentials button {\n    font-size: 30px;\n    display: inline;\n    position: absolute;\n    bottom: 0;\n    right: 0;\n}\n\n.logout {\n    font-size: 20px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../angular/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<form *ngIf=\"!api.loggedIn\" class=\"credentials\">\n    <input\n        [(ngModel)]=\"creds.email\"\n        type=\"email\"\n        name=\"email\"\n        placeholder=\"E-Mail\" />\n    <input\n        [(ngModel)]=\"creds.password\"\n        type=\"password\"\n        name=\"password\"\n        placeholder=\"Password\" />\n    <button type=\"submit\" (click)=\"login()\">Login</button>\n</form>\n<button *ngIf=\"api.loggedIn\" (click)=\"logout()\" class=\"logout\">Log Out</button>"

/***/ }),

/***/ "../../../../../angular/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_authentication__ = __webpack_require__("../../../../../angular/app/api/authentication.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notice__ = __webpack_require__("../../../../../angular/app/notice/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var LoginComponent = (function () {
    function LoginComponent(api, notice) {
        this.creds = { email: '', password: '' };
        this.loggedIn = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.api = api;
        this.notice = notice;
    }
    LoginComponent.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.api.login(this.creds)];
                    case 1:
                        _a.sent();
                        this.loggedIn.emit(true);
                        return [3 /*break*/, 3];
                    case 2:
                        _1 = _a.sent();
                        this.notice.ok('Failed to log in.', function () { });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.api.logout()];
                    case 1:
                        _a.sent();
                        this.loggedIn.emit(false);
                        return [3 /*break*/, 3];
                    case 2:
                        _2 = _a.sent();
                        this.notice.ok('Failed to log out.', function () { });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return LoginComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], LoginComponent.prototype, "loggedIn", void 0);
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'sr-login',
        template: __webpack_require__("../../../../../angular/app/login/login.component.html"),
        styles: [__webpack_require__("../../../../../angular/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_authentication__["a" /* Authentication */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__api_authentication__["a" /* Authentication */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__notice__["a" /* Notice */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__notice__["a" /* Notice */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../angular/app/notice/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Notice; });
var Notice = (function () {
    function Notice() {
    }
    return Notice;
}());

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../angular/app/notice/notice.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".overlay {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: rgb(0, 0, 0, 0.5);\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  left: 0;\n  position: fixed;\n  right: 0;\n  top: 0;\n}\n\n.box {\n  background: white;\n  border-radius: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  height: 260px;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 40px;\n  width: 410px;\n}\n\n.message {\n  font-size: 28px;\n}\n\n.buttons {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: right;\n      -ms-flex-pack: right;\n          justify-content: right;\n}\n\n.buttons button {\n  font-size: 20px;\n  margin: 0 20px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../angular/app/notice/notice.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\" *ngIf=\"notice.message\">\n  <div class=\"box\">\n    <p class=\"message\">{{ notice.message }}</p>\n    <div class=\"buttons\">\n      <button *ngIf=\"notice.fn1\" (click)=\"notice.fn1()\">\n        {{ notice.button1 }}\n      </button>\n      <button *ngIf=\"notice.fn2\" (click)=\"notice.fn2()\">\n        {{ notice.button2 }}\n      </button>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../angular/app/notice/notice.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__("../../../../../angular/app/notice/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NoticeComponent = (function () {
    function NoticeComponent(notice) {
        this.notice = notice;
    }
    return NoticeComponent;
}());
NoticeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'sr-notice',
        template: __webpack_require__("../../../../../angular/app/notice/notice.component.html"),
        styles: [__webpack_require__("../../../../../angular/app/notice/notice.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__index__["a" /* Notice */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__index__["a" /* Notice */]) === "function" && _a || Object])
], NoticeComponent);

var _a;
//# sourceMappingURL=notice.component.js.map

/***/ }),

/***/ "../../../../../angular/app/notice/notice.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NoticeService = (function () {
    function NoticeService() {
    }
    Object.defineProperty(NoticeService.prototype, "message", {
        get: function () { return this.m; },
        enumerable: true,
        configurable: true
    });
    NoticeService.prototype.ok = function (message, cb) {
        this.m = message;
        this.button1 = 'OK';
        this.button2 = undefined;
        this.fn1 = this.close(cb);
        this.fn2 = undefined;
    };
    NoticeService.prototype.confirm = function (message, yes, no) {
        this.m = message;
        this.button1 = 'OK';
        this.button2 = 'Cancel';
        this.fn1 = this.close(yes);
        this.fn2 = this.close(no);
    };
    NoticeService.prototype.close = function (fn) {
        var _this = this;
        return function () {
            _this.m = undefined;
            fn();
        };
    };
    return NoticeService;
}());
NoticeService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], NoticeService);

//# sourceMappingURL=notice.service.js.map

/***/ }),

/***/ "../../../../../angular/app/portfolio/portfolio.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n    color: darkgreen;\n    font-size: 42px;\n    font-weight: 100;\n    margin: 0;\n}\n\n.portfolios {\n    list-style-type: none;\n    margin: 40px 0;\n    padding: 0;\n}\n\n.portfolios li {\n    padding: 0 20px;\n}\n\n.portfolios li:nth-child(odd) {\n    background-color: #fafafa;\n}\n\n.name div {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    line-height: 50px;\n}\n\n.name form {\n    padding: 10px 0;\n    width: 100%;\n}\n\n.name form span {\n    float: right;\n}\n\n.investments {\n    width: 100%;\n    table-layout: fixed;\n}\n\n.investments td:nth-child(1) {\n    text-transform: uppercase;\n}\n\n.spinner {\n    display: inline-block;\n    height: 16px;\n    width: 16px;\n}\n\n.display-name {\n    color: darkgreen;\n    font-size: 20px;\n    font-weight: bold;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../angular/app/portfolio/portfolio.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\n  <h2 class=\"title\">My Portfolios</h2>\n  <button *ngIf=\"!editing || editing.id\" (click)=\"onCreate()\">\n      New Portfolio\n    </button>\n    <form *ngIf=\"editing && !editing.id\">\n      <input\n        type=\"text\" \n        name=\"name\" \n        placeholder=\"Name\" \n        [(ngModel)]=\"editing.name\" />\n      <button type=\"submit\" (click)=\"onCreateSubmit()\">Save</button>\n      <button (click)=\"onCancel()\">Cancel</button>\n    </form>\n</div>\n\n<ul class=\"portfolios\">\n  <li *ngFor=\"let p of portfolios.data\">\n    <div class=\"name\">\n      <div *ngIf=\"!editing || !editing.id || editing.id != p.id\">\n        <span class=\"display-name\">{{ p.name }}</span>\n        <span>\n          <button (click)=\"onEdit(p)\">Edit</button>\n          <button (click)=\"onDelete(p)\">Delete</button>\n        </span>\n      </div>\n      <form *ngIf=\"editing && editing.id && editing.id == p.id\">\n        <input\n          type=\"text\"\n          name=\"name\"\n          placeholder=\"Name\" \n          [(ngModel)]=\"editing.name\" />\n        <span>\n          <button type=\"submit\" (click)=\"onEditSubmit()\">Save</button>\n          <button (click)=\"onCancel()\">Cancel</button>\n        </span>\n      </form>\n    </div>\n    <table class=\"investments\">\n      <thead>\n        <tr>\n          <th>Symbol</th>\n          <th>Shares</th>\n          <th>Strike</th>\n          <th>Total Investment</th>\n          <th>Current Value</th>\n        </tr>\n      </thead>\n      <tbody class=\"data\">\n        <tr *ngFor=\"let i of p.investments\">\n          <td>{{ i.symbol }}</td>\n          <td>{{ i.shares | number : '.3-3' }}</td>\n          <td>{{ i.strike | currency : 'USD' : true }}</td>\n          <td>{{ i.strike * i.shares | currency : 'USD' : true }}</td>\n          <td>\n            <span *ngIf=\"i.bid\">\n              {{ i.bid * i.shares | currency : 'USD' : true }}\n            </span>\n            <span *ngIf=\"!i.bid\">\n              <button \n                (click)=\"onProfit(i)\" \n                [disabled]=\"waitingOn(i)\">\n                Calculate\n              </button>\n              <span class=\"spinner\" *ngIf=\"waitingOn(i)\"></span>\n            </span>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </li>\n</ul>\n"

/***/ }),

/***/ "../../../../../angular/app/portfolio/portfolio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortfolioComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_types__ = __webpack_require__("../../../../../angular/app/data/types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api__ = __webpack_require__("../../../../../angular/app/api/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notice_index__ = __webpack_require__("../../../../../angular/app/notice/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PortfolioComponent = (function () {
    function PortfolioComponent(api, portfolios, notice) {
        this.waitings = [];
        this.error = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.api = api;
        this.notice = notice;
        this.portfolios = portfolios;
    }
    PortfolioComponent.prototype.onCreate = function () {
        this.editing = { id: undefined, name: '', investments: [] };
    };
    PortfolioComponent.prototype.onEdit = function (p) {
        this.editing = this.portfolios.find(function (i) { return i.id === p.id; });
    };
    PortfolioComponent.prototype.onEditSubmit = function () {
        this.portfolios.update(this.editing);
        this.editing = undefined;
    };
    PortfolioComponent.prototype.onCreateSubmit = function () {
        this.portfolios.add(this.editing);
        this.editing = undefined;
    };
    PortfolioComponent.prototype.onDelete = function (p) {
        var _this = this;
        this.notice.confirm("Are you sure you want to delete " + p.name + "?", function () { return _this.portfolios.remove(p); }, function () { });
    };
    PortfolioComponent.prototype.onCancel = function () {
        this.editing = undefined;
    };
    PortfolioComponent.prototype.onProfit = function (i) {
        var _this = this;
        this.waitings.push({ investment: i, waiting: true });
        this.api.search(i.symbol).subscribe(function (data) {
            i.bid = data.bid;
            i.high = data.high;
            i.low = data.low;
            var idx = _this.waitings.findIndex(function (w) { return w.investment.id == i.id; });
            _this.waitings.splice(idx, 1);
        });
    };
    PortfolioComponent.prototype.waitingOn = function (i) {
        return !!this.waitings.find(function (w) { return w.investment.id == i.id; });
    };
    return PortfolioComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PortfolioComponent.prototype, "error", void 0);
PortfolioComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'sr-portfolio',
        template: __webpack_require__("../../../../../angular/app/portfolio/portfolio.component.html"),
        styles: [__webpack_require__("../../../../../angular/app/portfolio/portfolio.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__api__["a" /* API */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__api__["a" /* API */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__data_types__["a" /* Portfolios */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__data_types__["a" /* Portfolios */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__notice_index__["a" /* Notice */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__notice_index__["a" /* Notice */]) === "function" && _c || Object])
], PortfolioComponent);

var _a, _b, _c;
//# sourceMappingURL=portfolio.component.js.map

/***/ }),

/***/ "../../../../../angular/app/stock/stock.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search, .amount {\n    border: none;\n    padding: 0 10px;\n    width: calc(100% - 20px);\n}\n\n.search {\n    color: darkgreen;\n    font-size: 24px;\n    line-height: 50px;\n}\n\n.info {\n    margin: 20px 0;\n    text-align: center;\n    width: 100%;\n}\n\n.info thead {\n    font-size: 18px;\n}\n\n.invest {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    position: relative;\n    padding-bottom: 60px;\n    width: 100%;\n}\n\n.amount {\n    font-family: 'IBM Plex Mono Light', monospace;\n    font-size: 18px;\n    line-height: 40px\n}\n\n.invest p {\n    font-size: 20px;\n    font-weight: bold;\n    margin: 20px 0;\n    text-align: center;\n}\n\n.portfolio {\n    font-size: 18px;\n    padding: 10px;\n}\n\n.invest button {\n    bottom: 0;\n    font-size: 20px;\n    position: absolute;\n    right: 0;\n}\n\n.spinner {\n    width: 40px;\n    height: 40px;\n    margin: 60px auto;\n}\n\n.error {\n    font-size: 24px;\n    text-align: center;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../angular/app/stock/stock.component.html":
/***/ (function(module, exports) {

module.exports = "<input\n  type=\"text\"\n  name=\"search\"\n  class=\"search\"\n  placeholder=\"Search by Ticker Symbol\"\n  [formControl]=\"search\" />\n<div *ngIf=\"waiting\" class=\"spinner\"></div>\n<p *ngIf=\"notFound\" class=\"error\">Not found.</p>\n<div *ngIf=\"stock\">\n    <table class=\"info\">\n        <thead>\n            <tr>\n                <th>Bid</th>\n                <th>High</th>\n                <th>Low</th>\n            </tr>\n        </thead>\n        <tbody class=\"data\">\n            <tr>\n                <td>{{ stock.bid | currency : 'USD' : true }}</td>\n                <td>{{ stock.high | currency : 'USD' : true }}</td>\n                <td>{{ stock.low | currency : 'USD' : true }}</td>\n            </tr>\n        </tbody>\n    </table>\n    <form class=\"invest\">\n        <input type=\"number\" \n            name=\"amount\" \n            class=\"amount\"\n            step=\"0.01\"\n            placeholder=\"Investment Amount\" \n            [(ngModel)]=\"amount\" />\n        <p><span class=\"data\">{{ shares | number : '.3-3' }}</span> shares</p>\n        <select name=\"pid\" [(ngModel)]=\"pid\" class=\"portfolio\">\n            <option *ngFor=\"let p of portfolios.data\" [ngValue]=\"p.id\">\n                {{ p.name }}\n            </option>\n        </select>\n        <button type=\"submit\" (click)=\"onInvest()\">Invest</button>\n    </form>\n</div>"

/***/ }),

/***/ "../../../../../angular/app/stock/stock.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StockComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_types__ = __webpack_require__("../../../../../angular/app/data/types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api__ = __webpack_require__("../../../../../angular/app/api/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var StockComponent = (function () {
    function StockComponent(api, portfolios) {
        this.api = api;
        this.portfolios = portfolios;
        this.search = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]();
        this.search.valueChanges
            .debounceTime(500)
            .subscribe(this.onSearch.bind(this));
    }
    Object.defineProperty(StockComponent.prototype, "shares", {
        get: function () {
            if (!this.stock.bid)
                return 0;
            return this.amount / this.stock.bid || 0;
        },
        enumerable: true,
        configurable: true
    });
    StockComponent.prototype.onSearch = function (symbol) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.notFound = false;
                        if (!symbol) {
                            this.stock = undefined;
                            return [2 /*return*/];
                        }
                        this.pid = this.portfolios.data[0].id;
                        this.waiting = true;
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, 4, 5]);
                        _a = this;
                        return [4 /*yield*/, this.api
                                .search(symbol)
                                .toPromise()];
                    case 2:
                        _a.stock = _b.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        _1 = _b.sent();
                        this.notFound = true;
                        return [3 /*break*/, 5];
                    case 4:
                        this.waiting = false;
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    StockComponent.prototype.onInvest = function () {
        var _this = this;
        var investment = Object.assign(this.stock, {
            amount: this.amount,
            strike: this.stock.bid,
            shares: this.shares
        });
        var portfolio = this.portfolios.find(function (p) { return p.id == _this.pid; });
        this.portfolios.invest(portfolio, investment);
        this.search.setValue('');
        this.amount = undefined;
        this.stock = undefined;
    };
    return StockComponent;
}());
StockComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'sr-stock',
        template: __webpack_require__("../../../../../angular/app/stock/stock.component.html"),
        styles: [__webpack_require__("../../../../../angular/app/stock/stock.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__api__["a" /* API */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__api__["a" /* API */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__data_types__["a" /* Portfolios */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__data_types__["a" /* Portfolios */]) === "function" && _b || Object])
], StockComponent);

var _a, _b;
//# sourceMappingURL=stock.component.js.map

/***/ }),

/***/ "../../../../../angular/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../angular/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../angular/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../angular/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../angular/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map