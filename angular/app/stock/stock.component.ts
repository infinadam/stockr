import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import { Investment, Portfolios, Stock } from '../data/types';
import { API } from '../api';


@Component({
  selector: 'sr-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent {

  private api: API;
  private portfolios: Portfolios;
  private search: FormControl;
  private stock: Stock;
  private pid: number;

  private notFound: boolean;
  public waiting: boolean;

  private amount: number;
  private get shares(): number {
    if (!this.stock.bid) return 0;
    return this.amount / this.stock.bid || 0;
  }

  constructor (api: API, portfolios: Portfolios) {
    this.api = api;
    this.portfolios = portfolios;
    this.search = new FormControl();
    this.search.valueChanges
      .debounceTime(500)
      .subscribe(this.onSearch.bind(this));
  }

  async onSearch (symbol: string) {
    this.notFound = false;
    
    if (!symbol) {
      this.stock = undefined;
      return;
    }

    this.pid = this.portfolios.data[0].id;
    this.waiting = true;
    try {
    this.stock = await this.api
      .search(symbol)
      .toPromise();
    } catch (_) {
      this.notFound = true;
    } finally {
      this.waiting = false;
    }
  }

  onInvest () {
    const investment = Object.assign(this.stock, {
      amount: this.amount,
      strike: this.stock.bid,
      shares: this.shares
    });
    const portfolio = this.portfolios.find(p => p.id == this.pid);
    this.portfolios.invest(portfolio, investment);
    
    this.search.setValue('');
    this.amount = undefined;
    this.stock = undefined;
  }
}
