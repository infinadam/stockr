import { Component, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Investment, Portfolio, Portfolios } from '../data/types';
import { API } from '../api';
import { Notice } from '../notice/index';


interface IWaiting {
  investment: Investment;
  waiting: boolean;
}

@Component({
  selector: 'sr-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent {

  private api: API;
  private editing: Portfolio;
  private notice: Notice;
  private portfolios: Portfolios;

  public waitings: IWaiting[] = [];
  
  @Output() private error = new EventEmitter<string>();

  constructor (api: API, portfolios: Portfolios, notice: Notice) {
    this.api = api;
    this.notice = notice;
    this.portfolios = portfolios;
  }

  onCreate () {
    this.editing = { id: undefined, name: '', investments: [] };
  }

  onEdit (p: Portfolio) {
    this.editing = this.portfolios.find(i => i.id === p.id);
  }

  onEditSubmit () {
    this.portfolios.update(this.editing);
    this.editing = undefined;
  }

  onCreateSubmit () {
    this.portfolios.add(this.editing);
    this.editing = undefined;
  }

  onDelete (p: Portfolio) {
    this.notice.confirm(
      `Are you sure you want to delete ${p.name}?`,
      () => this.portfolios.remove(p),
      () => {}
    );
  }

  onCancel () {
    this.editing = undefined;
  }

  onProfit (i: Investment) {
    this.waitings.push({ investment: i, waiting: true });
    this.api.search(i.symbol).subscribe(data => {
      i.bid = data.bid;
      i.high = data.high;
      i.low = data.low;
      const idx = this.waitings.findIndex(w => w.investment.id == i.id);
      this.waitings.splice(idx, 1);
    });
  }

  waitingOn (i: Investment) {
    return !!this.waitings.find(w => w.investment.id == i.id);
  }
}
