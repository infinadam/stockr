import { Component, Input } from '@angular/core';

import { Notice } from './index';


@Component({
  selector: 'sr-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.css']
})
export class NoticeComponent {

  public notice: Notice;

  constructor (notice: Notice) {
    this.notice = notice;
  }
}
