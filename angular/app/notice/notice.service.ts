import { Injectable } from '@angular/core';

import { Notice } from './index';


@Injectable()
export class NoticeService implements Notice {

  private m: string;
  private button1: string;
  private button2: string;

  private fn1: () => void;
  private fn2: () => void;

  public get message (): string { return this.m; }

  constructor() { }

  public ok (message: string, cb: () => void): void {
    this.m = message;
    this.button1 = 'OK';
    this.button2 = undefined;
    this.fn1 = this.close(cb);
    this.fn2 = undefined;
  }

  public confirm (message: string, yes: () => void, no: () => void): void {
    this.m = message;
    this.button1 = 'OK';
    this.button2 = 'Cancel';
    this.fn1 = this.close(yes);
    this.fn2 = this.close(no);
  }

  private close (fn: () => void): () => void {
    return () => {
      this.m = undefined;
      fn();
    };
  }
}
