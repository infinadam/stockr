export abstract class Notice {
  public abstract ok (message: string, cb: () => void): void;
  public abstract confirm (
    message: string, yes: () => void, no: () => void
  ): void;
}