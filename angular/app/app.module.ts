import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NoticeComponent } from './notice/notice.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { StockComponent } from './stock/stock.component';

import { Authentication } from './api/authentication';
import { AuthenticationService } from './api/services/authentication.service';
import { API } from './api';
import { APIService } from './api/services/api.service';
import { Resources, LocalResources } from './api/resources';
import { Portfolios } from './data/types';
import { PortfoliosService } from './data/services/portfolios.service';
import { Notice } from './notice';
import { NoticeService } from './notice/notice.service';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        NoticeComponent,
        PortfolioComponent,
        StockComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    providers: [
        { provide: Authentication, useClass: AuthenticationService },
        { provide: Resources, useValue: LocalResources },
        { provide: API, useClass: APIService },
        { provide: Portfolios, useClass: PortfoliosService },
        { provide: Notice, useClass: NoticeService },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
