import { Injectable } from '@angular/core';

import { Investment, Portfolios, Portfolio } from '../types';
import { API } from '../../api/index';


@Injectable()
export class PortfoliosService implements Portfolios  {

  private portfolios: Portfolio[] = [];
  private api: API;

  public get data (): Portfolio[] {
    return this.portfolios;
  }

  constructor(api: API) {
    this.api = api;
  }

  public fill (): Portfolios {
    this.portfolios = [];
    this.api.portfolios()
      .subscribe(p => {
        this.portfolios.push(p);
        p.investments = [];
        this.api.investments(p.id).subscribe(i => {
          p.investments.push(i);
        });
      });
    return this as Portfolios;
  }

  public find (fn: (p: Portfolio) => boolean): Portfolio {
    return this.portfolios.find(fn);
  }

  public add (p: Portfolio): Portfolios {
    this.api.createPortfolio(p)
      .subscribe(data => this.portfolios.push(data));
    
    return this as Portfolios;
  }

  public update (p: Portfolio): Portfolios {
    this.api.updatePortfolio(p)
      .subscribe(u => {
        const idx = this.portfolios.findIndex(i => i.id == u.id);
        this.portfolios.splice(idx, 1, u);
      });
    return this as Portfolios;
  }

  public remove (p: Portfolio): Portfolios {
    this.api.deletePortfolio(p.id).subscribe(() => {
      const idx = this.portfolios.findIndex(i => i.id == p.id);
      this.portfolios.splice(idx, 1);
    });
    return this as Portfolios;
  }

  public invest (p: Portfolio, i: Investment): Portfolios {
    p.investments = p.investments || [];
    this.api.investIn(p.id, i.symbol, i.strike, i.shares)
      .subscribe(() => p.investments.push(i));
    return this as Portfolios;
  }
}
