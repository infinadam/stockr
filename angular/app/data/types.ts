import { Injectable } from '@angular/core';

import { API } from '../api';


export interface Investment extends Stock {
    id?: number;
    strike: number;
    shares: number;
}

export interface Stock {
    symbol: string;
    bid: number;
    high: number;
    low: number;
}

export interface Portfolio {
  id: number;
  name: string;
  investments: Investment[];
}

Injectable()
export abstract class Portfolios {
  public abstract get data (): Portfolio[];

  public abstract fill (): Portfolios;
  public abstract find (fn: (p: Portfolio) => boolean): Portfolio;
  public abstract add (p: Portfolio): Portfolios;
  public abstract update (p: Portfolio): Portfolios;
  public abstract remove (p: Portfolio): Portfolios;

  public abstract invest (p: Portfolio, i: Investment): Portfolios;
}
