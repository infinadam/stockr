import { Component, Output, EventEmitter } from '@angular/core';

import { Authentication } from '../api/authentication';
import { Credentials } from '../api/types';
import { Notice } from '../notice';


@Component({
  selector: 'sr-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private api: Authentication;
  private creds: Credentials = { email: '', password: '' };
  private notice: Notice;

  @Output() private loggedIn = new EventEmitter<boolean>();

  constructor (api: Authentication, notice: Notice) {
    this.api = api;
    this.notice = notice;
  }

  async login () {
    try {
      await this.api.login(this.creds);
      this.loggedIn.emit(true);
    } catch (_) {
      this.notice.ok('Failed to log in.', () => {});
    }
  }

  async logout () {
    try {
      await this.api.logout();
      this.loggedIn.emit(false);
    } catch (_) {
      this.notice.ok('Failed to log out.', () => {});
    }
  }
}
