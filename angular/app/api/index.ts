import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Investment, Stock, Portfolio } from '../data/types';

@Injectable()
export abstract class API {
    abstract portfolios (id?: number): Observable<Portfolio>;
    abstract createPortfolio (p: Portfolio): Observable<Portfolio>;
    abstract updatePortfolio (p: Portfolio): Observable<Portfolio>;
    abstract deletePortfolio (id: number): Observable<void>;

    abstract investments (pid: number): Observable<Investment>;
    abstract search (symbol: string): Observable<Stock>;

    abstract investIn (
      pid: number, symbol: string, strike: number, shares: number
    ): Observable<Investment>;
}
