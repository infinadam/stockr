import { Injectable } from '@angular/core';

import { Token } from './types/token';
import { Credentials } from './types';

@Injectable()
export abstract class Authentication {
    loggedIn: boolean;
    accessToken: string;

    abstract login(creds: Credentials): Promise<{}>;
    abstract logout(): Promise<{}>;
}
