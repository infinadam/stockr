export interface ApiToken {
    access_token: string;
    expires_in: number;
}

export class Token {
    private static storageKey = 'sr-token';

    public accessToken: string;
    public expiresAt: Date;

    constructor () {
        const data = JSON.parse(localStorage.getItem(Token.storageKey));

        if (data) {
            Object.assign(this, {
                accessToken: data.accessToken,
                expiresAt: new Date(data.expiresAt)
            });
        }
    }

    fromApi (apiToken: ApiToken) {
        this.accessToken = apiToken.access_token;
        this.expiresAt = new Date(Date.now() + apiToken.expires_in);

        return this;
    }

    store () {
        const token = {
            accessToken: this.accessToken,
            expiresAt: this.expiresAt.toString()
        };

        localStorage.setItem(Token.storageKey, JSON.stringify(token));

        return this;
    }

    forget () {
        this.accessToken = this.expiresAt = undefined;
        localStorage.removeItem(Token.storageKey);
    }

    get isValid () {
        return !!this.accessToken
            && !!this.expiresAt
            && this.expiresAt.getTime() > Date.now();
    }
}
