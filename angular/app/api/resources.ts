// Using instead of interfaces to avoid token injections.
export abstract class Resources {
    token: string;
    portfolio: string;
    investment: string;
    symbol: string;
}

export let LocalResources: Resources = {
    token: '/api/token',
    portfolio: '/api/portfolio/{id}',
    investment: '/api/portfolio/{pid}/investment/{id}',
    symbol: '/api/symbol/{name}',
};
