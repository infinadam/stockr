import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Credentials } from '../types';
import { Token, ApiToken } from '../types/token';
import { Authentication } from '../authentication';
import { Resources } from '../resources';


@Injectable()
export class AuthenticationService implements Authentication {

  private URL: string = '/api/token';

  private client: HttpClient;
  private token = new Token();

  constructor (client: HttpClient) {
    this.client = client;
  }

  login (creds: Credentials) {
    if (this.loggedIn) {
      return Promise.resolve(this.token);
    }

    return new Promise((resolve, reject) =>
      this.client
        .post<ApiToken>(this.URL, creds)
        .subscribe(data => resolve(this.token.fromApi(data).store()), reject)
    );
  }

  logout () {
    if (!this.loggedIn) {
      return Promise.reject('Not logged in.');
    }

    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${this.token.accessToken}`);

    return new Promise((resolve, reject) => {
      this.client
        .delete(this.URL, { headers })
        .subscribe(() => {
          this.token.forget();
          resolve();
        }, reject);
    });
  }

  get loggedIn () {
    return this.token.isValid;
  }

  get accessToken () {
    if (this.token.isValid)
      return this.token.accessToken;
    return undefined;
  }
}
