import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { API } from '../index';
import { Portfolio, Investment, Stock } from '../../data/types';

import { Authentication } from '../authentication';
import { Resources } from '../resources';


@Injectable()
export class APIService implements API {

  private http: HttpClient;
  private auth: Authentication;
  private resources: Resources;

  private get options (): { headers: HttpHeaders } {
    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${this.auth.accessToken}`);
    return { headers };
  }

  constructor(
    http: HttpClient,
    authentication: Authentication,
    resources: Resources
  ) {
    this.http = http;
    this.auth = authentication;
    this.resources = resources;
  }

  portfolios (id?: number): Observable<Portfolio> {
    let url;

    if (id) {
      url = this.resources.portfolio.replace('{id}', id.toString());
      return this.http.get<Portfolio>(url, this.options);
    }

    url = this.resources.portfolio.replace('/{id}', '');
    return this.http
      .get<Portfolio[]>(url, this.options)
      .flatMap(ps => Observable.from(ps));
  }

  createPortfolio (p: Portfolio): Observable<Portfolio> {
    const url = this.resources.portfolio.replace('/{id}', '');
    return this.http.post<Portfolio>(url, p, this.options);
  }

  updatePortfolio (p: Portfolio): Observable<Portfolio> {
    const url = this.resources.portfolio.replace('{id}', p.id.toString());
    return this.http.put<Portfolio>(url, p, this.options);
  }

  deletePortfolio (id: number): Observable<void> {
    const url = this.resources.portfolio.replace('{id}', id.toString());
    return this.http.delete<void>(url, this.options);
  }

  investments (pid: number): Observable<Investment> {
    const url = this.resources.investment
      .replace('{pid}', pid.toString())
      .replace('/{id}', '');

    return this.http
      .get<Investment[]>(url, this.options)
      .flatMap(i => Observable.from(i));
  }

  search (symbol: string): Observable<Stock> {
    const url = this.resources.symbol.replace('{name}', symbol);
    return this.http.get<Stock>(url);
  }

  investIn (
    pid: number, symbol: string, strike: number, shares: number
  ): Observable<Investment> {
    const url = this.resources.investment
      .replace('{pid}', pid.toString())
      .replace('/{id}', '');
    const investment = { symbol, strike, shares };

    return this.http.post<Investment>(url, investment, this.options);
  }
}
