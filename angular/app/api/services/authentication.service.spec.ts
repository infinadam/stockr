import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule, HttpTestingController 
} from '@angular/common/http/testing';

import { AuthenticationService } from './authentication.service';
import { Resources } from '../resources';
import { ApiToken } from '../types/token';

describe('ApiService', () => {
  let httpMock;
  let service;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService]
    });

    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(AuthenticationService);
  });

  it('should be created', () => {
    expect(httpMock).toBeTruthy();
    expect(service).toBeTruthy();
  });

  it('should log in.', (done) => {
    service.login({ email: 'test@test.com', password: 'test'})
      .then(() =>{
        httpMock.verify();
        done();
      });
    const req = httpMock.expectOne('/api/token');
    expect(req.request.method).toBe('POST');
    req.flush({ access_token: '', expires_in: 1 });
  });

  it('should log out.', (done) => {
    service.token.accessToken = 'test';
    service.token.expiresAt = new Date(Date.now() + 999999999);
    
    service.logout().then(() => {
      httpMock.verify();
      done();
    });
    const req = httpMock.expectOne('/api/token');
    expect(req.request.headers.get('Authorization')).toBe('Bearer test');
    expect(req.request.method).toBe('DELETE');
    req.flush({});
  });

  it('should not log in on HTTP error.', (done) => {
    service.login({ email: 'test@test.com', password: 'test'})
      .catch((e) =>{
        expect(e).toBeDefined();
        httpMock.verify();
        done();
      });
    const req = httpMock.expectOne('/api/token');
    req.flush({}, { status: 400, statusText: 'Unauthorized' });
  });

  it('should not log out on HTTP error.', (done) => {
    service.token.accessToken = 'test';
    service.token.expiresAt = new Date(Date.now() + 999999999);
    
    service.logout().catch((e) => {
      expect(e).toBeDefined();
      httpMock.verify();
      done();
    });
    const req = httpMock.expectOne('/api/token');
    expect(req.request.headers.get('Authorization')).toBe('Bearer test');
    req.flush({}, { status: 500, statusText: 'Internal Server Error' });
  }); 
});
