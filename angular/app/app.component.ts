import { Component, OnInit } from '@angular/core';

import { Authentication } from './api/authentication';
import { API } from './api';

import { Portfolio, Portfolios } from './data/types';


@Component({
    selector: 'sr-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private loggedIn: boolean;

  private authentication: Authentication;
  private api: API;
  private portfolios: Portfolios;

  constructor (
    authentication: Authentication, 
    api: API, 
    portfolios: Portfolios
  ) {
    this.authentication = authentication;
    this.api = api;
    this.portfolios = portfolios;
  }

  onAuthChange (loggedIn: boolean) {
    this.loggedIn = loggedIn;
    if (this.loggedIn) this.onLogin();
    else this.onLogout();
  }

  ngOnInit () {
    this.loggedIn = this.authentication.loggedIn;
    if (this.loggedIn) this.onLogin();
    else this.onLogout();
  }

  private onLogin () {
    this.portfolios.fill();
  }

  private onLogout () {}
}
